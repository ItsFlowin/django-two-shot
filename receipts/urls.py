from django.urls import path, include

from receipts.views import (
    ReceiptListView,
    ReceiptCreateView,
    ExpenseCategoryListView,
    ExpenseCategoryCreateView,
    AccountListView,
    AccountCreateView,
)

urlpatterns = [
    path("", ReceiptListView.as_view(), name="home"),
    path("create/", ReceiptCreateView.as_view(), name="create_receipt"),
    path("categories/", ExpenseCategoryListView.as_view(), name="cat_list"),
    path("accounts/", AccountListView.as_view(), name="acc_list"),
    path(
        "categories/create/",
        ExpenseCategoryCreateView.as_view(),
        name="cat_new",
    ),
    path("accounts/create/", AccountCreateView.as_view(), name="create_account"),
]
